#ifndef _incrementalNEval_h
#define _incrementalNEval_h

#include <vector>
#include "solution.h"
#include "MixedQAPEval.h"
#include "neighborhoodEval.h"

class IncrementalNEval : public NeighborhoodEval {
	public:
		IncrementalNEval(MixedQAPeval & _eval) : eval(_eval) { }
		
		virtual void init(Solution & _solution, std::vector<std::vector<double>> & delta) {}

		virtual void update(Solution & _solution, std::pair<unsigned int, unsigned int> & neighbor, std::vector<std::vector<double>> & delta) {
			unsigned int tmp;
            unsigned int u,v;

			if(neighbor.first < neighbor.second){
				u = neighbor.first;
				v = neighbor.second;
			} else {
				u = neighbor.second;
				v = neighbor.first;
			}

			double sum = 0;

			for (int k=0; k<u; k++)
				sum += ((_solution.flow[u][k] - _solution.flow[v][k]) * (eval.distance[_solution.sigma[v]][_solution.sigma[k]] - eval.distance[_solution.sigma[u]][_solution.sigma[k]]));
			for (int k = u+1; k < v; k++)
				sum += ((_solution.flow[k][u] - _solution.flow[v][k]) * (eval.distance[_solution.sigma[v]][_solution.sigma[k]] - eval.distance[_solution.sigma[u]][_solution.sigma[k]]));
			for(int k = v+1 ; k < eval.n ; k++) 
				sum += ((_solution.flow[k][u] - _solution.flow[k][v]) * (eval.distance[_solution.sigma[v]][_solution.sigma[k]] - eval.distance[_solution.sigma[u]][_solution.sigma[k]]));
				
			_solution.fitness += 2*sum;
		}

	protected:
		MixedQAPeval & eval;
};

#endif