 #ifndef _neighborhoodEval_h_
#define _neighborhoodEval_h_

#include "solution.h"

class NeighborhoodEval
{
public:
        virtual void init(Solution &_solution,
                std::vector<std::vector<double>> &delta) = 0;
        virtual void update(Solution &_solution,
                std::pair<unsigned int, unsigned int> &neighbor,
                std::vector<std::vector<double>> &delta) = 0;
};

#endif