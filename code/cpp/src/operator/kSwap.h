#ifndef _kswap_h_
#define _kswap_h_

#include "operator.h"
#include "solution.h"
#include "mixedQAPeval.h"
#include "randomPermutation.h"
#include <random>

class KSwap : public Operator {
    public:
        KSwap(std::default_random_engine & _rng) : rng(_rng){}

        void operator()(Solution & solution) {
            std::uniform_int_distribution<unsigned> distMax(0, solution.sigma.size());
            unsigned int kMax = distMax(rng);
            std::uniform_int_distribution<unsigned> distMin(0, kMax);
            unsigned int kMin = distMin(rng);
            std::uniform_int_distribution<unsigned> dist(kMin, kMax);
            unsigned int k = dist(rng);
            RandomPermutation rp(rng);
            for (unsigned int i = 0 ; i < k ; i++)
                rp(solution);
        }
    
    protected:
        std::default_random_engine & rng;
};

#endif