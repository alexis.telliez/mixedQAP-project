#ifndef _randomPermutation_h
#define _randomPermutation_h

#include "operator.h"
#include "solution.h"
#include <random>
#include <algorithm>

class RandomPermutation : public Operator {
    public:
        RandomPermutation(std::default_random_engine & _rng) : rng(_rng) {}

        void operator()(Solution & solution) {

            std::uniform_int_distribution<unsigned int> dist(0, solution.sigma.size()-1);
            unsigned int i = dist(rng);
            unsigned int j = dist(rng);;

            while(i == j)
                j = dist(rng);

            std::swap(solution.sigma[i], solution.sigma[j]);
			
        }
    
    protected:
        std::default_random_engine & rng;
};

#endif