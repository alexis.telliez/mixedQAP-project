#ifndef _operator_h_
#define _operator_h_

#include "solution.h"

class Operator {
    public:
        virtual void operator()(Solution & _solution) = 0;
};

#endif