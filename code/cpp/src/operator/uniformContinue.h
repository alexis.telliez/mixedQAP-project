#ifndef _uniformContinue_h_
#define _uniformContinue_h_

#include "operator.h"

class UniformContinue : public Operator {

    public:
        UniformContinue(unsigned int _n) : n(_n) { }
        void operator()(Solution & solution) {
            solution.sigma.resize(n);
            solution.x.resize(n);
            for(unsigned i = 0; i < n; i++) {
                solution.x[i] = 1.0 / n;
                solution.sigma[i] = i;
            }
                
            solution.modifiedX = true;
        }
    protected:
        unsigned int n;
};


#endif