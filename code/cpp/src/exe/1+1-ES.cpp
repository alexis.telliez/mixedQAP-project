/*
./es ../../../../../instances/mixedQAP_uni_20_1_100_1.dat  1 3
*/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <random>

#include "solution.h"
#include "mixedQAPeval.h"
#include "RandomPermutation.h"
#include "UniformContinue.h"
#include "FullNEval.h"
#include "1+1-ES.h"

using namespace std;

/**
 *  Main function
 */
int main(int argc, char **argv) {
   //---------------------------------
    // *** Arguments

    // file instance name
    char * fileName = argv[1];

    // random seed
    unsigned int seed = atoi(argv[2]);

    // time stopping criterium (in second)
    unsigned int duration = atoi(argv[3]);

    //---------------------------------

    auto rng = std::default_random_engine {};

    rng.seed(seed);    

    // evaluation function
    MixedQAPeval eval(fileName);
    FullNEval fullNEval(eval);

    // Uniform initial of the contiunous variable
    UniformContinue uniform(eval.n);

    // ILS
    ES es(eval, eval.n, rng);
    //init time
    es.timeLimit(time(NULL) + duration);

    // precision print
    std::cout.precision(15);

    //---------------------------------

    Solution solution(eval.n);

    uniform(solution);    

    eval(solution);
	es.timeLimit(time(NULL) + duration);
    cout << "AVANT 1+1-ES" << endl;
    cout << solution << endl;
    es(solution);
    cout << "APRES 1+1-ES" << endl;
    cout << solution << endl;
    //----------------------------------
    // ok

    return 1;
}
