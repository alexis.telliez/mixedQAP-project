/*
./ils ../../../../../instances/mixedQAP_uni_6_1_100_1.dat  1 3
*/

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <random>

#include "solution.h"
#include "mixedQAPeval.h"
#include "RandomPermutation.h"
#include "UniformContinue.h"
#include "FullNEval.h"
#include "ils.h"

using namespace std;

/**
 *  Main function
 */
int main(int argc, char **argv) {
   //---------------------------------
    // *** Arguments

    // file instance name
    char * fileName = argv[1];

    // random seed
    unsigned int seed = atoi(argv[2]);

    // time stopping criterium (in second)
    unsigned int duration = atoi(argv[3]);

    //---------------------------------

    auto rng = std::default_random_engine {};

    rng.seed(seed);    

    // evaluation function
    MixedQAPeval eval(fileName);
    FullNEval fullNEval(eval);
    
    //KSwap operator
    KSwap kswap(rng);

    // Uniform initial of the contiunous variable
    UniformContinue uniform(eval.n);

    // ILS
    ILS ils(eval,fullNEval, eval.n, kswap);
    //init time
    ils.timeLimit(time(NULL) + duration);

    // precision print
    std::cout.precision(15);

    //---------------------------------

    Solution solution(eval.n);

    //uniform(solution);    

    for(unsigned int i = 0; i < solution.x.size(); i++) {
        solution.x[i] = 1.0 / eval.n;
        solution.sigma[i] = i;
    }

    eval(solution);
    cout << "AVANT ILS" << endl;
    cout << solution << endl;
    ils(solution);
    cout << "APRES ILS" << endl;
    cout << solution << endl;

    //----------------------------------
    // ok

    return 1;
}