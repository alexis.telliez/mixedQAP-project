/*
./hillClimber ../../../../../instances/mixedQAP_uni_20_1_100_1.dat 3
*/

#include <iostream>
#include <fstream>
#include <stdlib.h>

#include "solution.h"
#include "mixedQAPeval.h"
#include "FullNEval.h"
#include "hillClimberBestImprovment.h"
#include "incrementalNEval.h"

using namespace std;

int main(int argc, char **argv) {
    // *** Arguments

    // file instance name
    char * fileName = argv[1]; 

    // time stopping criterium (in second)
    unsigned int duration = atoi(argv[2]);

    // evaluation function
    MixedQAPeval eval(fileName);

    FullNEval fullNEval(eval);
    //IncrementalNEval incrementalNEval(eval);

	// HC 
    HillClimberBestImprovment hillClimber(eval, fullNEval, eval.n);

    Solution solution(eval.n);
    
    // precision print
    std::cout.precision(15);

    //---------------------------------

    hillClimber.timeLimit(time(NULL) + duration);
    
	for(unsigned int i = 0; i < solution.x.size(); i++) {
            solution.x[i] = 1.0 / eval.n;
            solution.sigma[i] = i;
    }

    eval(solution);

    cout << "Solution avant HC: "<< endl << solution << endl;

    hillClimber(solution);
    
    cout << "Solution après HC: "<< endl << solution << endl;  

    return 1;
}
