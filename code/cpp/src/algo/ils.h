#ifndef _ils_h_
#define _ils_h_

#include "neighborhoodEval.h"
#include "mixedQAPeval.h"
#include "search.h"
#include "kSwap.h"

class ILS : public Search{

	public:
		ILS(MixedQAPeval & _eval, NeighborhoodEval & _neighborhoodEval, unsigned int _n, KSwap & _kswap) : eval(_eval), neighborhoodEval(_neighborhoodEval), n(_n), kswap(_kswap) {
			delta.resize(n);

			for(unsigned int i = 0 ; i < n ; i++)
				delta[i].resize(n);
		}
	

		virtual void operator()(Solution & _solution) {
			bool localOpt = false;
			std::pair<unsigned int, unsigned int> move;
			
			localSearch(_solution, move);
			int cpt = 0;
			while(time(NULL) < timeLimit_) {	
				Solution copy = _solution;
				kswap(copy);
				localSearch(copy, move);
				if(copy.fitness < _solution.fitness) _solution = copy;
				cpt++;				
			}
			std::cout << "COMPTEUR : " << cpt << std::endl;
		}

		void localSearch(Solution & _solution, std::pair<unsigned int, unsigned int> move) {
			neighborhoodEval.init(_solution, delta);
			selectBestNeighbor(move);
			if(delta[ move.first ][ move.second ] < 0) {
				unsigned int tmp = _solution.sigma[move.first];
				_solution.sigma[move.first] = _solution.sigma[move.second];
				_solution.sigma[move.second] = tmp;
				_solution.fitness += delta[ move.first ][ move.second ];
			}
		}
		
	protected:
		virtual void selectBestNeighbor(std::pair<unsigned int, unsigned int> & best) {
			unsigned int i, j;

			best.first = 1;
			best.second = 0;
			double bestDelta = delta[ best.first ][ best.second ];

			for(i = 1; i < n ; i++) {
				for(j= 0; j < i ; j++) {
					if(delta[i][j] < bestDelta) {
						best.first = i;
						best.second = j;
						bestDelta = delta[i][j];
					}
				}
			}
		}

		MixedQAPeval & eval;
		NeighborhoodEval & neighborhoodEval;
		unsigned n;
		std::vector<std::vector<double>> delta;
		KSwap & kswap;
};

#endif