#ifndef _randomSearch_h_
#define _randomSearch_h_

#include <random>
#include "mixedQAPeval.h"
#include "search.h"
#include "randomPermutation.h"
#include "uniformContinue.h"

class RandomSearch : public Search {
    public:
        RandomSearch(std::default_random_engine & _rng, MixedQAPeval & _eval):rng(_rng), eval(_eval) { }

        virtual void operator()(Solution & _solution) {
            RandomPermutation random(rng);
            UniformContinue uniform(_solution.x.size());
            Solution s(_solution.sigma.size());
            uniform(s);
            while(time(NULL) < timeLimit_) {
                random(s);
                eval(s);
                if (s.fitness < _solution.fitness)
                    _solution = s;
            }
        }

    protected:
        std::default_random_engine & rng;
        MixedQAPeval & eval;
};

#endif