#ifndef _hillClimberBestImprovment_h_
#define _hillClimberBestImprovment_h_

#include "neighborhoodEval.h"
#include "mixedQAPeval.h"
#include "search.h"

class HillClimberBestImprovment : public Search{
	
	public:
		HillClimberBestImprovment(MixedQAPeval & _eval, NeighborhoodEval & _neighborhoodEval, unsigned _n) : eval(_eval), neighborhoodEval(_neighborhoodEval), n(_n) {
			delta.resize(n);

			for(unsigned int i = 0 ; i < n ; i++)
				delta[i].resize(n);
		}

		virtual void operator()(Solution & _solution) {
			bool localOpt = false;
			std::pair<unsigned int, unsigned int> move;
			neighborhoodEval.update(_solution, move, delta);
			int cpt = 0;
			while(!localOpt && time(NULL) < timeLimit_) {
				
				selectBestNeighbor(move);
				
				if(delta[ move.first ][ move.second ] < 0) {
					unsigned tmp = _solution.sigma[move.first];
					_solution.sigma[move.first] = _solution.sigma[move.second];
					_solution.sigma[move.second] = tmp;

					_solution.fitness += delta[ move.first ][ move.second ];

					neighborhoodEval.update(_solution, move, delta);

				} else {
					localOpt = true;
				}
				cpt ++;
			}
			std::cout << "COMPTEUR : " << cpt << std::endl;
		}

	protected:
		virtual void selectBestNeighbor(std::pair<unsigned int, unsigned int> & best) {
			unsigned int i, j;

			best.first = 1;
			best.second = 0;
			double bestDelta = delta[ best.first ][ best.second ];

			for(i = 1; i < n ; i++) {
				for(j= 0; j < i ; j++) {
					if(delta[i][j] < bestDelta) {
						best.first = i;
						best.second = j;
						bestDelta = delta[i][j];
					}
				}
			}
		}

		MixedQAPeval & eval;
		NeighborhoodEval & neighborhoodEval;
		unsigned n;
		std::vector<std::vector<double>> delta;
};


#endif