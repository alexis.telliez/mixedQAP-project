#include "search.h"
#include "MixedQAPeval.h"
#include "NeighborhoodEval.h"
#include <vector>
#include <iostream>
#include <cmath>
#include <random>

class ES : public Search {
	public:
		ES(MixedQAPeval & _eval, unsigned _n, std::default_random_engine & _rng): eval(_eval), n(_n), rng(_rng) { }

		virtual void operator() (Solution & _solution) {
			int cpt = 0;
			std::pair<unsigned int, unsigned int> move;
			int last200Iter = 0;
			double step = 1;
			double gamma = 2;
			double lastFitness = 0;
			std::uniform_real_distribution<double> dist(-1.0, 1.0);

			while(last200Iter < 200) {
				Solution output = _solution;
				std::vector<double> variation;
				for (int i=0; i<_solution.x.size(); i++) {
					variation.push_back(dist(rng));
					output.x.at(i) = _solution.x.at(i) + step * variation.at(i);
				}

                // To manage constraints, use the following repair method
				double sum = 0;
				for (int i = 0 ; i < output.x.size() ; i++) {
					if (output.x.at(i) < 0) output.x.at(i) *= -1.0;
					sum += output.x.at(i);
                }
                if (sum == 0){
                    for (int i = 0 ; i < output.x.size() ; i++) output.x.at(i) = 1/output.x.size();
                } 
                else if (sum != 1){
                    for (int i = 0 ; i < output.x.size() ; i++) output.x.at(i) = output.x.at(i) / sum;
                }
				// true when the vector x have been modified
				output.modifiedX = true;
                
                eval(_solution);
                eval(output);

				if (output.fitness < _solution.fitness) {
					_solution = output;
					step *= gamma;
				} else step *= pow(gamma, -1.0/4.0);

				if (std::abs(_solution.fitness - lastFitness) < 10E-6) last200Iter++;
				else last200Iter = 0;

				lastFitness = _solution.fitness;
				cpt++;
			}
			std::cout << "COMPTEUR ES :" << cpt << std::endl;
		}

	protected:
		MixedQAPeval & eval;
		unsigned n;
		std::default_random_engine & rng;
};
